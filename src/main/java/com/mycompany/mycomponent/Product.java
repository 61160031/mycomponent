/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mycomponent;

import java.util.ArrayList;

/**
 *
 * @author Tramboliko
 */
public class Product {

    private int id;
    private String name;
    private double price;
    private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }

    public static ArrayList<Product> genProductList() {
        ArrayList<Product> lsit = new ArrayList<>();
        lsit.add(new Product(1, "coffee 1", 40, "1.jpg"));
        lsit.add(new Product(1, "coffee 2", 50, "2.jpg"));
        lsit.add(new Product(1, "coffee 3", 60, "3.jpg"));
        lsit.add(new Product(1, "coffee 4", 10, "4.jpg"));
        lsit.add(new Product(1, "coffee 5", 20, "5.jpg"));
        lsit.add(new Product(1, "coffee 6", 30, "6.jpg"));
        return lsit;
    }

}
